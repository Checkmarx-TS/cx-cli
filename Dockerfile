FROM openjdk:8

RUN \
  apt-get update && \
  apt-get install -y curl git unzip vim wget

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

WORKDIR /cx
COPY . /cx

RUN curl -L https://download.checkmarx.com/8.8.0/Plugins/CxConsolePlugin-8.80.0.zip -o ./cx.zip && unzip cx.zip -d .
RUN chmod u+x /cx/CxConsolePlugin-8.80.0/runCxConsole.sh
ENTRYPOINT ["/cx/CxConsolePlugin-8.80.0/runCxConsole.sh"]
